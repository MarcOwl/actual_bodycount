﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ButtonsManager : MonoBehaviour
{

    public GameObject pcCreation;
    public Button pCButton;

    public InputField pCName;
    public InputField pCTitle;
    public InputField pCT;
    public InputField pCD;
    public InputField pCA;

    public GameObject greenHorn;
    GameObject preFoetus;

    public Transform adventurerTab;

    public GameObject statBlockManager;

    //GameObject preFoetus;

    public GameObject inputName;
    public GameObject inputTitle;

    public GameObject nameRef;
    public GameObject titleRef;
    public GameObject tRef;
    public GameObject dRef;
    public GameObject aRef;




    void Awake()
    {
        pcCreation.SetActive(false);
        statBlockManager.SetActive(false);
    }


    public void CreatePC()
    {
        pcCreation.SetActive(true);
        pCButton.GetComponent<Button>().interactable = false;
    }

    public void ConfirmCreation()
    {
        if (pCName.text != "")
        {

            pCButton.GetComponent<Button>().interactable = true;


            preFoetus = (GameObject)Instantiate(greenHorn, transform);

            preFoetus.name = pCName.text.ToString();
            preFoetus.GetComponentInChildren<Text>().text = pCName.text.ToString();



            preFoetus.AddComponent<PushStatBlock>();
            preFoetus.GetComponent<PushStatBlock>().nyme = pCName.text.ToString();
            preFoetus.GetComponent<PushStatBlock>().title = pCTitle.text.ToString();
            preFoetus.GetComponent<PushStatBlock>().t = pCT.text.ToString();
            preFoetus.GetComponent<PushStatBlock>().d = pCD.text.ToString();
            preFoetus.GetComponent<PushStatBlock>().a = pCA.text.ToString();


    preFoetus.GetComponent<PushStatBlock>().statBlock = statBlockManager;
            preFoetus.GetComponent<PushStatBlock>().nameGoesHere = nameRef;
            preFoetus.GetComponent<PushStatBlock>().titleGoesHere = titleRef;

            preFoetus.GetComponent<PushStatBlock>().tGoesHere = tRef;
            preFoetus.GetComponent<PushStatBlock>().dGoesHere = dRef;
            preFoetus.GetComponent<PushStatBlock>().aGoesHere = aRef;




            pcCreation.SetActive(false);

            this.GetComponent<BraveAdventurer>().CreateFoetus();



            pCName.text = "";
            pCTitle.text = "";
        }
    }


}
